#!/bin/bash

for directory in `find . -maxdepth 1 -mindepth 1 -type d`
do
	for file in ${directory}/*
	do
 		echo "Processing ${file} file..."
		structName=$(basename "$file")
		extension="${structName##*.}"
		structName="${structName%.*}"
		sed -e "s/LT_FILE/${structName}/g" align_template.tcl > ./${directory}/align.tcl
		cd ${directory}
		vmd -dispdev text -e align.tcl
		rm align.tcl 
		cd ..
	done   
done
