proc center_of_mass {selection} {
        # some error checking
        if {[$selection num] <= 0} {
                error "center_of_mass: needs a selection with atoms"
        }
        # set the center of mass to 0
        set com [veczero]
        # set the total mass to 0
        set mass 0
        # [$selection get {x y z}] returns the coordinates {x y z} 
        # [$selection get {mass}] returns the masses
        # so the following says "for each pair of {coordinates} and masses,
	#  do the computation ..."
        foreach coord [$selection get {x y z}] m [$selection get mass] {
           # sum of the masses
           set mass [expr $mass + $m]
           # sum up the product of mass and coordinate
           set com [vecadd $com [vecscale $m $coord]]
        }
        # and scale by the inverse of the number of atoms
        if {$mass == 0} {
                error "center_of_mass: total mass is zero"
        }
        # The "1.0" can't be "1", since otherwise integer division is done
        return [vecscale [expr 1.0/$mass] $com]
}

proc geom_center {selection} {
        # set the geometrical center to 0
        set gc [veczero]
        # [$selection get {x y z}] returns a list of {x y z} 
        #    values (one per atoms) so get each term one by one
        foreach coord [$selection get {x y z}] {
           # sum up the coordinates
           set gc [vecadd $gc $coord]
        }
        # and scale by the inverse of the number of atoms
        return [vecscale [expr 1.0 /[$selection num]] $gc]
}

# load molecule
mol new LT_FILE.xyz
set allAtoms [atomselect 0 "all"]

# calculate vector between Ir atoms
set Ir1 [atomselect 0 "index 0"]
foreach elem [$Ir1 get { x y z }] {
    set vIr1 $elem
} 

set Ir2 [atomselect 0 "index 103"]
foreach elem [$Ir2 get { x y z }] {
    set vIr2 $elem
}

set vDir [vecsub $vIr1 $vIr2]

# align molecule along z
set mRot [transvecinv $vDir]
$allAtoms move $mRot
set mRot [transaxis y 90]
$allAtoms move $mRot

# bring the molecule to its geometric centre
set vector_to_centre [geom_center $allAtoms]
$allAtoms moveby $vector_to_centre
animate write xyz LT_FILE_align_rec.xyz

quit
