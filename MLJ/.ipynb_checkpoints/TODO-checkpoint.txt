# Todo 
 1)Test rotazione qr=q1*q2’ → qr*q2 = q1
          - Lo scalare è identico
          - Il quaternione "differenza" è inverso: [q0, q1, q2, q3] --> [q0, -q1, -q2, -q3]
 2) Definizione di q[], di q, rmsd 
         - q = q1 * conjugate(q2)
         - q[0], q[1], q[2], q[3] = 2*arccos(|q1 * conj(q2)'|)
 3) Output: coupling; test logaritmo

 4) Aggiungere qualche esempio del test (nei vari gruppi)
         - Fatto (da discutere insieme)
 5) Indicare numero dati
         - Total: 1252,
         - New  : 929,
         - Old  : 323,
 6) Istogramma distribuzione nel training set/test set/predetta
         - Fatto, ma mi sa che andiamo maluccio (però forse devi capire meglio che ci sta sugli assi)
 7) Correlazione (sia lineare che con log(coupling))
         - Ma non è la stessa cosa di 3) ?
 8) Controllare orientazione molecole per q=0: sono allineate con l’asse principale lungo l’asse z (o un altro asse cartesiano). --> Abbiamo visto di sì con 14_192 (credo)
 9) Controllare (=visualizzare)la rotazione delle molecole (?) per un certo quaternione differenza fisso.
         - Test fatto, i quaternioni funzionano: presa molecola ideale, ruotata di angolo (x°, y°,            z°) e poi calcolato quaternione fra molecola ideale e molecola ideale ruotata 
           --> risultato corretto
10) Controllare/rifare grafici correlazione (?)
11) Fare verifica delle rotazioni --> scegliere una rotazione, scriverla come quaternione, applicarla ad una molecola e poi vederle entrambe (originale e ruotata) e vedere se effettivamente è stata ruotata dell'angolo giusto
        - Fatto per 9)
12) Prese due molecole, vedo il quaternione "differenza" e vedo se quello mi porta la molecola 1 nella 2 (o viceversa)
        - Fatto per 9)
13) Classi di resto sui quaternioni! --> che poi in realtà è un normale isomorfismo dallo spazio delle rotazioni a [0, 120)...